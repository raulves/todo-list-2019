<?php

class TodoItem {
    public $id;
    public $name;
    public $dateAdded;
    public $completed;
    public $comments;

    public function __construct($name, $dateAdded = null, $completed = false, $id = null) {
        $this->name = $name;
        $this->completed = $completed;
        if ($dateAdded) {
            $this->dateAdded = $dateAdded;
        } else {
            $this->dateAdded = date('Y-m-d');
        }
        $this->id = $id;
        $this->comments = [];
    }

    public function __toString() {
        return "TodoItem{id: $this->id, name: $this->name, completed: $this->completed, dateAdded: $this->dateAdded}";
    }

    public function addComment($comment) {
        $this->comments[] = $comment;
    }
}