<?php

const USERNAME = "raulves";
const PASSWORD = "67db";
const URL = "mysql:host=db.mkalmo.xyz;dbname=raulves";

function addTodoItem($todoItem) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("insert into todo_item (name, date_added, completed) 
        values (:name, :dateAdded, :completed)");
    $statement->bindValue(":name", $todoItem->name);
    $statement->bindValue(":dateAdded", $todoItem->dateAdded);
    $statement->bindValue(":completed", (int)$todoItem->completed);

    $statement->execute();
    $todoItemId = $connection->lastInsertId();

    foreach ($todoItem->comments as $comment) {
        $statement = $connection->prepare("insert into comment (todo_item_id, content) values(:todoItemId, :content)");
        $statement->bindValue(":todoItemId", $todoItemId);
        $statement->bindValue(":content", $comment);
        $statement->execute();
    }
}

function getTodoItems() {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("select ti.id, ti.name, ti.date_added, ti.completed, c.content from todo_item ti
        left join comment c on c.todo_item_id = ti.id");
    $statement->execute();

    $todoItems = [];
    foreach ($statement as $row) {
        $id = $row["id"];

        if (isset($todoItems[$id])) {
            $todoItem = $todoItems[$id];
            $comment = $row["content"];
            $todoItem->addComment($comment);
        } else {
            $name = $row["name"];
            $dateAdded = $row["date_added"];
            $completed = $row["completed"];
            $comment = $row["content"];
            $todoItem = new TodoItem($name, $dateAdded, $completed, $id);
            $todoItem->addComment($comment);
        }

        $todoItems[$id] = $todoItem;
    }

    return array_values($todoItems);
}

function completeTodoItem($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("update todo_item set completed = true where id = :id");
    $statement->bindValue(":id", $id);

    $statement->execute();
}

function deleteTodoItem($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("delete from todo_item where id = :id");
    $statement->bindValue(":id", $id);

    $statement->execute();
}

function getTodoItemById($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("select ti.id, ti.name, ti.date_added, ti.completed, c.content from todo_item ti
        left join comment c on c.todo_item_id = ti.id
        where ti.id = :id");
    $statement->bindValue(":id", $id);
    $statement->execute();

    $todoItem = null;
    foreach ($statement as $row) {
        $id = $row["id"];

        if (isset($todoItem)) {
            $comment = $row["content"];
            $todoItem->addComment($comment);
        } else {
            $name = $row["name"];
            $dateAdded = $row["date_added"];
            $completed = $row["completed"];
            $comment = $row["content"];
            $todoItem = new TodoItem($name, $dateAdded, $completed, $id);
            $todoItem->addComment($comment);
        }
    }

    return $todoItem;
}
