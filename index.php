<?php
require_once("vendor/tpl.php");
require_once("TodoItem.php");
require_once("mysqlTodoList.php");

$cmd = "view";
if (isset($_GET["cmd"])) {
    $cmd = $_GET["cmd"];
}

if ($cmd === "view") {
    $todoItems = getTodoItems();
    $data = ['todoItems' => $todoItems, 'subTemplate' => "list.html"];
    print renderTemplate("tpl/main.html", $data);
} else if ($cmd === "add") {
    $todoItemName = $_POST["todo-item-name"];
    $todoItem = new Todoitem($todoItemName);
    addTodoItem($todoItem);

    $allItems = getTodoItems();
    $data = ['todoItems' => $allItems, 'subTemplate' => 'list.html'];
    print renderTemplate("tpl/main.html", $data);
} else if ($cmd === "delete") {
    $id = $_POST["id"];
    deleteTodoItem($id);

    $allItems = getTodoItems();
    $data = ['todoItems' => $allItems, 'subTemplate' => 'list.html'];
    print renderTemplate("tpl/main.html", $data);
} else if ($cmd === "complete") {
    $id = $_POST["id"];
    completeTodoItem($id);

    $allItems = getTodoItems();
    $data = ['todoItems' => $allItems, 'subTemplate' => 'list.html'];
    print renderTemplate("tpl/main.html", $data);
} else if ($cmd = "viewItem") {
    $itemId = $_GET["id"];
    $todoItem = getTodoItemById($itemId);

    $data = ['todoItem' => $todoItem, 'subTemplate' => 'todoItem.html'];
    print renderTemplate("tpl/main.html", $data);
}

